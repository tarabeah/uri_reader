<?PHP
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

include 'src/Reader.php';

$uriPattern[''] = [
			'prefix' => [
						'lang' => ['ar', 'en'],
						'country' => '*',
						'theam' => ['yesllo', 'black']
					],
			'alias' => 
				    [
				        'GET' => '/icore/world/1/2/3/5/page.html',
				        'load' => 'home/index',
				        'params' => [],
				        'name' => 'myPage'
				    ],
				    [
				        'GET' => '/alias/$digit.id/$alpha.name/$any.any/$string.file',
				        'load' => 'home/index/page',
				        'params' => [],
				        'name' => 'page-name'
				    ],
				    [
				        'GET' => '/models.html',
				        'load' => 'model/index',
				        'params' => [],
				        'name' => 'yourpage'
				    ],
];

$uriPattern['admin'] = [
	'prefix' => [
					'lang' => ['ar', 'en']
				]
];



$uri = new \Icore\Uri\Reader($uriPattern);
$uri->read();

