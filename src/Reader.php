<?PHP
/*
 * UriReader used to perform any procedure on the request(URI or CLI)
 * that include specify:
 *      read url,
 *      port name, prefix data,
 *      uri parameters segment,
 *      create uri base on rules,
 *      check url pattern matching.
 *      ... etc
 *
 * NOTE:
 *  uriReader not responsible for specify controller and action name, even though their
 *  are part of the url, because the controller and action should be implemented using
 *  file system level, so that will be under router level and pass them to uriReader
 *  if needed.
*/

namespace Icore\Uri;

Class Reader {

	private $config = [];
	private $GET = [];
	private $port = '';
	private $prefix = [];
	private $restUriParts = '';
    private $webPortRouter = null;

	/**
     * @set uri config
    */
	function __construct($config, $webPortRouter){
        $this->webPortRouter = $webPortRouter;
		$this->config = $config;
	}

    /**
     * @set serialize of base icore without config
     * @access public
     * @return void
    */
	function read(){

  		$SERVER = $this->getServerInfo();
  		$uriData = [];

    	if (!isset($SERVER['REQUEST_URI']) OR !isset($SERVER['SCRIPT_NAME'])) {
        	return '';
    	}

    	$uri = $SERVER['REQUEST_URI'];


        if (strpos($uri, $SERVER['SCRIPT_NAME']) === 0) {
            $uri = substr($uri, strlen($SERVER['SCRIPT_NAME']));
        } elseif (strpos($uri, dirname($SERVER['SCRIPT_NAME'])) === 0) {
            $uri = substr($uri, strlen(dirname($SERVER['SCRIPT_NAME'])));
        }

        // This section confirm that the even on servers that require the URI to be in the
        // query string (Nginx) with correct URI , and also fixes the
        // QUERY_STRING server var and $_GET array.
        if (strncmp($uri, '?/', 2) === 0) {
            $uri = substr($uri, 2);
        }

		$uriData = $this->getPartsOfUriAndPassedVariables($uri);

        // Decode the uri for non english parts
        if($uriData['uri']){
            foreach ($uriData['uri'] as $k=>$v){
                $v = urldecode($v);
                $uriData['uri'][$k] = $v;
            }
        }
        
		$this->GET = $uriData['GET'];
		$uriWithoutPort = $this->setPortAndReturnTheRest($uriData['uri']);
		$uriWithoutPrefix = $this->setPrefixAndReturnTheRest($uriWithoutPort);


		$this->restUriParts = $uriWithoutPrefix;
	}

	/**
     * Parse GET and URI
     *
     * parsing the URI and associative array of variables passed to the current script
     *
     * @access	private
     * @return	array
     */
    private function getPartsOfUriAndPassedVariables(string $uri): array {
        $parts = preg_split('#\?#i', $uri, 2);
        $uri = $parts[0];
        if (isset($parts[1]))
        {
            $_SERVER['QUERY_STRING'] = $parts[1];
            parse_str($_SERVER['QUERY_STRING'], $get);
        }
        else
        {
            $_SERVER['QUERY_STRING'] = '';
            $get =  [];
        }

        if ($uri == '/' || empty($uri))
        {
            $uri = '/';
        }

        $uri = parse_url($uri, PHP_URL_PATH);

        // Do some final cleaning of the URI and return it
        $uri = str_replace(array('//', '///', '////', '/////', '', '../'), '/', trim($uri, '/'));
        $uri = str_replace(array('///'), '/', trim($uri, '/'));
        $uri = str_replace(array('//'), '/', trim($uri, '/'));

        if($uri == ''){
        	$uri = [];
        } else {
        	$uri = explode('/', $uri);
        }

        return array('uri' => $uri, 'GET' => $get);
    }

    /**
     * @get $_SERVER info, this method used to make the mocking is doable.
     * @access private
     * @return array
    */
	private function getServerInfo(){
		$return = [];
        $return['SCRIPT_NAME'] = $_SERVER['SCRIPT_NAME'];
        $return['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
     	$return['SCRIPT_NAME'] = $_SERVER['SCRIPT_NAME'];

     	return $return;
	}

	/**
     * @Set the $this->port value and return the rest or uri
     * @access private
     * @return array
    */
	private function setPortAndReturnTheRest($uri){

		if(!$uri){
            $this->port = 'main';
			return [];
		}

		if(in_array($uri[0], array_keys($this->config))){
			$this->port = $uri[0];
			unset($uri[0]);
		} else {
            $this->port = 'main';
        }

		return $uri;
	}

	/**
     * @Set the $this->prefix value and return the rest or uri segments
     * @access private
     * @return array
    */
	private function setPrefixAndReturnTheRest($uriWithoutPort){

        $port = $this->port;

        if($port == 'web'){
            $port = $this->webPortRouter;
        }

		$prefixSegments = $this->config[$port]['prefix'];

        if(!$prefixSegments){
            return [];
        }

        $defaultPrefixValues = [];
        foreach($prefixSegments as $prefixK=>$prefixV){
            if($prefixV == '*'){
                $prefixV = [''];
            }

            $defaultPrefixValues[$prefixK] = $prefixV[0];
        }

        // set the default values of prefix keys
        $this->prefix = $defaultPrefixValues;

		// if there is no segments then return []
		if(!$uriWithoutPort){
			return [];
		}

        // reset the segment keys
        $uriWithoutPort = array_values($uriWithoutPort);

		$count = 0;
		foreach($prefixSegments as $prefixKey => $prefixValues){

			if($prefixValues == '*'){
                $this->prefix[$prefixKey] = isset($uriWithoutPort[$count]) ? $uriWithoutPort[$count] : '';
			} else {
				if(!isset($uriWithoutPort[$count])){
                    // There is no passed value, then keep it as is (default value)
                    $count++;
					break;
				} else {
					if(in_array($uriWithoutPort[$count], $prefixValues)){
						$this->prefix[$prefixKey] = $uriWithoutPort[$count];
					} else {
						$this->prefix[$prefixKey] = $prefixValues[0];
					}
				}
			}
			unset($uriWithoutPort[$count]);
			$count++;
		}

		return array_values($uriWithoutPort);
	}

    /**
     * @Read uri data that was read from uri by read()
     * @access public
     * @return array
     */
    public function getData(){
        return [
            'port' => $this->port,
            'prefix' => $this->prefix,
            'segments' => $this->GET,
            'GET' => $this->GET,
            'restUriParts' => $this->restUriParts
        ];
    }
}



